import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <h1> My First Angular App</h1>

  <button class="my-btn" [class.extraclass]="someProperty">Call to Action</button>
  <button class="my-btn" [style.border]="someProperty ? '5px solid yellow' : 'none'">Call to Action</button>
  <br> <br>

  <button class="my-btn" [ngClass]="setClasses()">Call to Action</button>
  <button class="my-btn" [ngStyle]="setStyles()">Call to Action</button>  
  
  `,
  styles: [`
  .my-btn{font-size:1.7em;}
  .extraclass { background: black; color: white;}
  .anotherclass { font-weight: bold; }
  `]
})
export class AppComponent {
  someProperty = true;
  anotherProperty = true;  

  setClasses(){
    let classes = {
      extraclass: this.someProperty,
      anotherclass: this.anotherProperty
    };
    return classes;
  }

  setStyles(){
    let styles = {
      'font-style': this.someProperty ? 'italic' : 'normal',
      'font-weight': this.anotherProperty ? 'bold' : 'normal'
    };
    return styles;
  }

}
